package cn.remex.core.exception;

public class FileException extends NestedException {

	private static final long serialVersionUID = -4066745120401393995L;

	public FileException(String errorCode, String msg) {
		super(errorCode, msg);
	}

	public FileException(String errorCode, String msg, Throwable cause) {
		super(errorCode, msg, cause);
	}


}
