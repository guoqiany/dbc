/* 
* 文 件 名 : NetException.java
* CopyRright (c) since 2013: 
* 文件编号： 
* 创 建 人：Liu Hengyang Email:yangyang8599@163.com QQ:119316891
* 日    期： 2013-6-17
* 修 改 人： 
* 日   期： 
* 描   述： 
* 版 本 号： 1.0
*/ 

package cn.remex.core.exception;

/**
 * @author Hengyang Liu  yangyang8599@163.com
 * @since 2013-6-17
 *
 */
public class NetException extends NestedException {

	private static final long serialVersionUID = 3862624615087073544L;

	public NetException(String errorCode, String msg) {
		super(errorCode, msg);
	}

	public NetException(String errorCode, String msg, Throwable cause) {
		super(errorCode, msg, cause);
	}


}
