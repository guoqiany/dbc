package cn.remex.core.exception;

public class StringHandleException extends NestedException {

	private static final long serialVersionUID = -4732372171741578021L;

	public StringHandleException(String errorCode, String msg) {
		super(errorCode, msg);
	}

	public StringHandleException(String errorCode, String msg, Throwable cause) {
		super(errorCode, msg, cause);
	}

}
