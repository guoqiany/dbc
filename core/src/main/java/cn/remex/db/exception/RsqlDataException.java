package cn.remex.db.exception;

public class RsqlDataException extends RsqlException {

	public RsqlDataException(String errorCode, String msg) {
		super(errorCode, msg);
	}

	public RsqlDataException(String errorCode, String msg, Throwable cause) {
		super(errorCode, msg, cause);
	}
}
