package cn.remex.db.utils;

/**
 *	判断一个对象是不是空
 */
public class Judgment {
	/**
	 * 判断一个对象是null或""
	 * @param vStr java对象
	 * @return boolean 布尔值
	 */
	public static boolean nullOrBlank (Object vStr){
		if(null == vStr || "".equals(vStr) )
			return true;
		return false;
	}

}
