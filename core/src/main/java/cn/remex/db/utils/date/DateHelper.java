package cn.remex.db.utils.date;

import cn.remex.db.utils.Assert;
import cn.remex.db.utils.Judgment;
import cn.remex.db.utils.exception.ServiceCode;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class DateHelper {

    private final static String formatDefault = "yyyy-MM-dd HH:mm:ss";

    /*
     * 万能的日期转化
     * Created by guoqi on 2016/6/27.
     */
    public static LocalDateTime parse(String dateTimeStr) {
        LocalDateTime localDateTime = null;
        //先匹配日期时间
        for (DateTimeFormatter dateTimeFormatter : DateFomatters.dateTimeFormatterList) {
            if (Judgment.nullOrBlank(localDateTime)) {
                try {
                    localDateTime = LocalDateTime.parse(dateTimeStr, dateTimeFormatter);
                } catch (Exception ignored) {
                }
            } else {
                break;
            }
        }
        //再匹配日期
        for (DateTimeFormatter dateTimeFormatter : DateFomatters.dateFormattersList) {
            if (Judgment.nullOrBlank(localDateTime)) {
                try {
                    localDateTime = LocalDate.parse(dateTimeStr, dateTimeFormatter).atStartOfDay();
                } catch (Exception ignored) {
                }
            } else {
                break;
            }
        }
        Assert.notNull(localDateTime, ServiceCode.FAIL, "时间格式错误,不符合默认的几个格式，请自己格式化");
        return localDateTime;
    }

    /*
     *  获取当前时间字符串
     *      返回一个格式为 yyyy-MM-dd HH:mm:ss
     */
    public static String getNow(String... format) {
        LocalDateTime localDateTime = LocalDateTime.now();
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(format == null || format.length < 1 ? formatDefault : format[0]);
        return localDateTime.format(dateTimeFormatter);

    }

    /*
     * 格式化 localdate
     *      默认返回 yyyy-MM-dd
     */
    public static String formatDate(LocalDate localDate, String... format) {
        if (null == format || format.length < 1)
            return localDate.toString();
        else {
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(format[0]);
            return localDate.format(dateTimeFormatter);
        }
    }
    /*
     * 格式化 localdate
     *      默认返回 yyyy-MM-dd
     */
    public static Date formatDate(String dateStr, String format) {
        Date date = null;
        try {
            date = new SimpleDateFormat(format).parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    /*
     * 格式化 localdateTime
     */
    public static String formatDate(LocalDateTime localDateTime, String... format) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(null == format || format.length < 1 ? formatDefault : format[0]);
        return localDateTime.format(dateTimeFormatter);
    }

    /**
     * date 转化为 LocalDateTime
     *
     * @param date 被转化的date
     * @return 返回转化后的 日期
     * @
     */
    public static LocalDateTime dateToLocalDateTime(Date date) {
        return LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
    }

    /*
     *  date 转化为LocalDate
     *  java.util.Date --&gt; java.time.LocalDate
     */
    public static LocalDate dateToLocalDate(Date date) {
        return dateToLocalDateTime(date).toLocalDate();
    }


    /**
     * localdate 转化为 date
     *
     * @param localDate 被转化的日期
     * @return 转化后的date
     */
    public static Date localDateToDate(LocalDate localDate) {
        Assert.notNull(localDate, ServiceCode.FAIL, "转换时间为空！");
        Instant instant = localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant();
        return Date.from(instant);
    }

    /**
     * localDateTime 转化为 date
     *
     * @param localDateTime 被转化的 日期
     * @return 返回date
     */
    public static Date localDateTimeToDate(LocalDateTime localDateTime) {
        Assert.notNull(localDateTime, ServiceCode.FAIL, "转换时间为空！");
        Instant instants = localDateTime.atZone(ZoneId.systemDefault()).toInstant();
        return Date.from(instants);
    }



}
