package cn.remex.db.utils.reflect;

/**
 * 名称：
 * 缩写：
 * 用途：
 * Created by yangy on 2016/12/31 0031.
 */
public interface CommConstructor<ParentObject, FieldInParent, ReturnType> {
	ReturnType create(ParentObject root, FieldInParent fieldName);
}
