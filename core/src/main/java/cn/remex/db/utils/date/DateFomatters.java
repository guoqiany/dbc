package cn.remex.db.utils.date;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by guoqi on 2016/8/18.
 */
class DateFomatters {
    private final static String[] dateTimeFomatterStr = {"yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm","yyyyMMddHHmm"};
    private final static String[] dateFomatterStr = {"yyyy-MM-dd"};

    static List<DateTimeFormatter> dateTimeFormatterList;
    static List<DateTimeFormatter> dateFormattersList;
    static {
        dateTimeFormatterList = new ArrayList<DateTimeFormatter>(){
            private static final long serialVersionUID = -2785155946948903268L;
            {
                add(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
//                add(DateTimeFormatter.ISO_DATE_TIME);
            }};
        for (String s : dateTimeFomatterStr) {
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(s);
            dateTimeFormatterList.add(dateTimeFormatter);
        }
        //日期格式
        dateFormattersList = new ArrayList<DateTimeFormatter>(){
            private static final long serialVersionUID = -361941930830852084L;
            {
                add(DateTimeFormatter.ISO_DATE);
            }};
        for (String s : dateFomatterStr) {
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(s);
            dateFormattersList.add(dateTimeFormatter);
        }
    }
}
