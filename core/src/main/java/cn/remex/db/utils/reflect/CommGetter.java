package cn.remex.db.utils.reflect;

/**
 * 名称：
 * 缩写：
 * 用途：
 * Created by yangy on 2016/12/31 0031.
 */
public interface CommGetter<Target, FieldName, ReturnType> {
	ReturnType visit(Target root, FieldName fieldName);
}
