package cn.remex.db.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * @author lanchenghao
 * @since 2013-7-23下午7:50:27
 * 系统当前状态变量，如session，用户，线程等
 */
public class CoreSvo {
	public final static ThreadLocal<Map<String, Object>> localParams = new ThreadLocal<Map<String, Object>>();
	/**
	 * 将值存入当前线程
	 *
	 * @param key   key
	 * @param value value
	 */
	public static void putLocal(String key, Object value) {
		Map<String, Object> map = localParams.get();
		if (null == map) {
			map = new HashMap<>();
			localParams.set(map);
		}
		map.put(key, value);
	}


	/**
	 * 将值从当前线程取出
	 *
	 * @param key key
	 * @return Object
	 */
	public static Object valLocal(String key) {

		Map<String, Object> map = localParams.get();
		if (null == map) {
			map = new HashMap<>();
			localParams.set(map);
		}
		return map.get(key);
	}
}
