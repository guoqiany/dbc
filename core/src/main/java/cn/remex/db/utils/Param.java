package cn.remex.db.utils;

import java.io.Serializable;

/**
 * Created by yangy on 2016/1/14 0014.
 */
public class Param<T> implements Serializable{
    public T param;

    public Param() {}
    public Param(T o) {
        this.param = o;
    }
}
