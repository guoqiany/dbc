package cn.remex.db.utils.exception;

/**
 * 名称：
 * 缩写：
 * 用途：
 * Created by yangy on 2016/4/23 0023.
 */
public interface ServiceCode {
    String ERROR = "ERROR";
    String FAIL = "FAIL";

    //db
    String RSQL_INIT_ERROR = "RSQL_INIT_ERROR";
    String RSQL_CONNECTION_ERROR = "RSQL_CONNECTION_ERROR";
    String RSQL_BEANSTATUS_ERROR = "RSQL_BEANSTATUS_ERROR";
    String RSQL_BEANCLASS_ERROR = "RSQL_BEANCLASS_ERROR";
    String RSQL_SQL_ERROR = "RSQL_SQL_ERROR";
    String RSQL_EXECUTE_ERROR = "RSQL_SQL_ERROR";
    String RSQL_QUERY_ERROR = "RSQL_QUERY_ERROR";
    String RSQL_UPDATE_ERROR = "RSQL_EXECUTE_ERROR";
    String RSQL_CREATECALL_ERROR = "RSQL_CREATECALL_ERROR";
    String RSQL_DIALECT_ERROR = "RSQL_DIALECT_ERROR";
    String RSQL_DATA_ERROR = "RSQL_DATA_ERROR";
    String RSQL_ERROR = "RSQL_ERROR";

    String RSQL_DATA_INVALID = "RSQL_DATA_INVALID";

}
