package cn.remex.db.utils.cache;

import cn.remex.RemexConstants;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

/**
 * 数据缓存池,是一个泛型类，泛型类型必须实现DataCacheCloneable接口
 *
 * @author HengYang Liu
 * @since 2014-6-11
 */
public class DataCachePool {

    private static Map<String, Map<Object, Object>> caches = new HashMap<>();


    /**
     * 根据缓存类型和名称获取对应的缓存对象
     * 根据缓存类型和名称获取对应的缓存对象
     *
     * @param type 缓存类型
     * @param key  缓存关键字，如缓存名称
     * @return Object 缓存对象
     */
    public static Object get(String type, Object key) {
        Map<Object, Object> cache = caches.get(type);
        return null == cache ? null : caches.get(type).get(key);
    }

    /**
     * 将一个对象放置指定类型和指定名称的缓存区中
     * 将一个对象放置指定类型和指定名称的缓存区中
     *
     * @param type  缓存池类型
     * @param key   缓存关键字，用来标志一个对象
     * @param value 缓存对象
     */
    public static void put(String type, Object key, Object value) {
        Map<Object, Object> cache = caches.get(type);
        if (null == cache) {
            cache = new HashMap<>();
            caches.put(type, cache);
        }
        cache.put(key, value);
    }

}
