package cn.remex.db.utils.aop;

import org.aopalliance.aop.Advice;
import org.springframework.aop.framework.ProxyFactory;

/**
 * @author guoqi
 * @date 2017/10/16
 */
public class DynamicProxy {

    public static<T> T getProxy(Class<T> clazz,Advice advice) {
        //实例化Spring代理工厂
        ProxyFactory factory = new ProxyFactory();
        //设置被代理的对象
        factory.setTargetClass(clazz);
        try {
            T t = clazz.newInstance();
            factory.setTarget(t);
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        //添加通知，横切逻辑
        factory.addAdvice(advice);

        return (T) factory.getProxy();
    }

}

