package cn.remex.db.model;

import cn.remex.db.rsql.model.ModelableImpl;
import cn.remex.db.rsql.transactional.RsqlDefine;

import javax.persistence.Column;

@RsqlDefine(renameFrom = "Info")
public class SysArticle extends ModelableImpl {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private SysArticleClass sysArticleClass;
	@Column(length = 100, columnDefinition = "CLOB")
	private String infoContent;
	@Column(length = 100, columnDefinition = "CLOB")
	private String infoSummary;
	@RsqlDefine(renameFrom = "indexShowTest")
	private boolean indexShow;//主页显示
	@Column(length = 25)
	private String publishDate;
	@Column(length = 200)
	private String showImg;
	private int showOrder;
	@Column(length = 100)
	private String src;
	@Column(length = 80)
	private String title;
	public boolean isIndexShow() {
		return indexShow;
	}
	public void setIndexShow(boolean indexShow) {
		this.indexShow = indexShow;
	}
	public SysArticleClass getSysArticleClass() {
		return this.sysArticleClass;
	}
	public String getInfoContent() {
		return this.infoContent;
	}
	public String getPublishDate() {
		return this.publishDate;
	}

	public String getShowImg() {
		return this.showImg;
	}
	public int getShowOrder() {
		return this.showOrder;
	}
	public String getSrc() {
		return this.src;
	}
	public String getTitle() {
		return this.title;
	}
	public void setSysArticleClass(final SysArticleClass sysArticleClass) {
		this.sysArticleClass = sysArticleClass;
	}
	public void setInfoContent(final String infoContent) {
		this.infoContent = infoContent;
	}
	public void setPublishDate(final String publishDate) {
		this.publishDate = publishDate;
	}
	public void setShowImg(final String showImg) {
		this.showImg = showImg;
	}
	public void setShowOrder(final int showOrder) {
		this.showOrder = showOrder;
	}
	public void setSrc(final String src) {
		this.src = src;
	}
	public void setTitle(final String title) {
		this.title = title;
	}
	public String getInfoSummary() {
		return infoSummary;
	}
	public void setInfoSummary(String infoSummary) {
		this.infoSummary = infoSummary;
	}


}
