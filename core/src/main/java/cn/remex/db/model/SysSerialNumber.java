/* 
* 文 件 名 : SysVariables.java
* CopyRright (c) since 2013: 
* 文件编号： 
* 创 建 人：Liu Hengyang Email:yangyang8599@163.com QQ:119316891
* 日    期： 2013-6-23
* 修 改 人： 
* 日   期： 
* 描   述： 
* 版 本 号： 1.0
*/

package cn.remex.db.model;

import cn.remex.core.exception.NestedException;
import cn.remex.db.Database;
import cn.remex.db.DbRvo;
import cn.remex.db.rsql.model.ModelableImpl;
import cn.remex.db.sql.WhereRuleOper;
import cn.remex.db.utils.exception.ServiceCode;

import java.util.UUID;

/**
 * @author Hengyang Liu  yangyang8599@163.com
 * @since 2013-6-23
 * 此表是用来保存主键的。为了避免主键冲突而设计
 */
public class SysSerialNumber extends ModelableImpl {
    public static final String createSerialNumber_oracle = "CREATE OR REPLACE FUNCTION\r\n" +
            "       \"createSerialNumber\" \r\n" +
            "          (beanName in \"SysSerialNumber\".\"beanName\"%type,\r\n" +
            "           fieldName in \"SysSerialNumber\".\"fieldName\"%type)\r\n" +
            "RETURN INTEGER\r\n" +
            "IS\r\n" +
            "       currentValue integer := 0;  --定义返回变量\r\n" +
            " \r\n" +
            "PRAGMA AUTONOMOUS_TRANSACTION;\r\n" +

            "BEGIN\r\n" +

            "    --最大数加1\r\n" +
            "    UPDATE \"SysSerialNumber\" SET \"currentValue\" = \"currentValue\"+1 where \"beanName\" = beanName and \"fieldName\" = fieldName\r\n" +
            "    Returning \"currentValue\" Into currentValue; --取出最大数\r\n" +

            "    If(SQL%NOTFOUND) THEN  --第一次向数据库中插入最大数为 1 的记录\r\n" +
            "       INSERT INTO \"SysSerialNumber\"(\"id\",\"beanName\",\"fieldName\",\"currentValue\") values('SSN'|| to_char(sysdate,'yyyyMMddhhmmss')||ABS(MOD(DBMS_RANDOM.RANDOM,10000)),beanName,fieldName,1) ;\r\n" +
            "       currentValue := 1;\r\n" +
            "    End If ;\r\n" +
            "     commit;\r\n" +
            "  return(currentValue); --返回结果\r\n" +
            "end \"createSerialNumber\";";

    public static final String querySerialNumber_oracle = "SELECT \"createSerialNumber\"(:beanName,:fieldName) from dual";


    /*
     *
     *
"CREATE OR REPLACE FUNCTION\r\n"+
"       \"createSerialNumber\" \r\n"+
"          (beanName in \"SysSerialNumber\".\"beanName\"%type,\r\n"+
"           fieldName in \"SysSerialNumber\".\"fieldName\"%type)\r\n"+
"RETURN INTEGER\r\n"+
"IS\r\n"+
"       currentValue integer := 0;  --定义返回变量\r\n"+
" \r\n"+
"PRAGMA AUTONOMOUS_TRANSACTION;\r\n"+

"BEGIN\r\n"+

"    --最大数加1\r\n"+
"    UPDATE \"SysSerialNumber\" SET \"currentValue\" = \"currentValue\"+1 where \"beanName\" = beanName and \"fieldName\" = fieldName\r\n"+
"    Returning \"currentValue\" Into currentValue; --取出最大数\r\n"+

"    If(SQL%NOTFOUND) THEN  --第一次向数据库中插入最大数为 1 的记录\r\n"+
"       INSERT INTO \"SysSerialNumber\"(\"id\",\"beanName\",\"fieldName\",\"currentValue\") values('SSN'|| to_char(sysdate,'yyyyMMddhhmmss')||ABS(MOD(DBMS_RANDOM.RANDOM,10000)),beanName,fieldName,1) ;\r\n"+
"       currentValue := 1;\r\n"+
"    End If ;\r\n"+
"     commit;\r\n"+
"  return(currentValue); --返回结果\r\n"+
"end \"createSerialNumber\";"
    ==================================================================
CREATE OR REPLACE FUNCTION
       "createSerialNumber"
          (beanName in "SysSerialNumber"."beanName"%type,
           fieldName in "SysSerialNumber"."fieldName"%type)
RETURN INTEGER
IS
       currentValue integer := 0;  --定义返回变量

PRAGMA AUTONOMOUS_TRANSACTION;

BEGIN

    --最大数加1
    UPDATE "SysSerialNumber" SET "currentValue" = "currentValue"+1 where "beanName" = beanName and "fieldName" = fieldName
    Returning "currentValue" Into currentValue; --取出最大数

    If(SQL%NOTFOUND) THEN  --第一次向数据库中插入最大数为 1 的记录
       INSERT INTO "SysSerialNumber"("id","beanName","fieldName","currentValue") values('SSN'|| to_char(sysdate,'yyyyMMddhhmmss')||ABS(MOD(DBMS_RANDOM.RANDOM,10000)),beanName,fieldName,1) ;
       currentValue := 1;
    End If ;
     commit;
  return(currentValue); --返回结果
end "createSerialNumber";
     *
     *
     *
     */

    public static Object createUID() {
        String uuid = UUID.randomUUID().toString();
        return uuid.substring(uuid.lastIndexOf("-") + 1, uuid.length());
    }

//    public static Object createSerialNumber(final Class<?> beanClass, String fieldName) {
//        HashMap<String, Object> params = new HashMap<String, Object>();
//        String beanName = StringHelper.getClassSimpleName(beanClass);
//        params.put("beanName", beanName);
//        params.put("fieldName", fieldName);
//
//        Container session = Database.getSession();
//        try {
//
//            return session.executeQuery(RDBManager.getLocalSpaceConfig().getDialect().obtainQuerySerialNumberFunctionSQL(), params).getCell(0, 0);
//        } catch (Exception e) {
//            if (StringHelper.match(e.getCause().getMessage(), "(ORA-00904)|(ORA-06575)", null) != null
//                    ||
//                    (e.getCause() instanceof MySQLSyntaxErrorException && ((MySQLSyntaxErrorException) e.getCause()).getSQLState().equals("42000"))
//                    ) {
//                Database.getSession().createCall(RDBManager.getLocalSpaceConfig().getDialect().obtainCreateSerialNumberFunctionSQL());
//                return session.executeQuery(RDBManager.getLocalSpaceConfig().getDialect().obtainQuerySerialNumberFunctionSQL(), params).getCell(0, 0);
//            } else {
//                throw e;
//            }
//        }
//
////
////		String where1 = " WHERE "	+d.quoteKey("beanName") + " = :beanName1"
////				+(null==fieldName?"":" AND "+d.quoteKey("fieldName")+" = :fieldName1");
////		String where2 = " WHERE "	+d.quoteKey("beanName") + " = :beanName2"
////				+(null==fieldName?"":" AND "+d.quoteKey("fieldName")+" = :fieldName2");
////
////		String updateSql = "UPDATE "+K_SysPrimaryKey
////				+" SET "+K_currentValue+" = ((select "+K_currentValue+" from "+K_SysPrimaryKey+where1+")+1)"
////				+where2;
////		if(0==session.executeUpdate(updateSql,params).getUpdateResult().getEffectRowCount()){
////			SysSerialNumber ssn = new SysSerialNumber();
////			ssn.setBeanName(beanName);
////			ssn.setFieldName(fieldName);
////			ssn.setCurrentValue("1");
////			Database.getSession().store(ssn);
////			String insertSql = "INSERT INTO "+SysPrimaryKey+" ("+d.quoteKey("id")+","
////																+d.quoteKey("beanName")+","
////																+d.quoteKey("fieldName")+","
////																+d.quoteKey("currentValue")+")VALUES("
////																+d.quoteAsString((new SysSerialNumber()).generateId())+","
////																+d.quoteAsString(beanName)+","
////																+d.quoteAsString(null==fieldName?"":fieldName)+","
////																+"1)";
////			RsqlExecutor.executeUpdate(insertSql);
////    }
//
//    }
    public static Object createSerialNumber(String beanName, String fieldName) {
        int times = 3;
        while (times-->0){
            SysSerialNumber sysSerialNumber = Database.select(SysSerialNumber.class)
                    .filterBy(SysSerialNumber::getBeanName, WhereRuleOper.equal, beanName)
                    .filterBy(SysSerialNumber::getFieldName, WhereRuleOper.equal, fieldName)
                    .execute().obtainBean();
            if (sysSerialNumber == null) {
                sysSerialNumber = new SysSerialNumber();
                sysSerialNumber.setCurrentValue("0");
                sysSerialNumber.setBeanName(beanName);
                sysSerialNumber.setFieldName(fieldName);
                Database.store(sysSerialNumber).execute().assertEffectOneRow(ServiceCode.FAIL, "序列号初始化冲突");
            }


            int cur = Integer.parseInt(sysSerialNumber.getCurrentValue()) + 1;
            DbRvo<SysSerialNumber> updateDbRvo = Database.update(SysSerialNumber.class)
                    .assignColumn(SysSerialNumber::getCurrentValue, cur)
                    .filterBy(SysSerialNumber::getBeanName, WhereRuleOper.equal, beanName)
                    .filterBy(SysSerialNumber::getFieldName, WhereRuleOper.equal, fieldName)
                    .filterBy(SysSerialNumber::getCurrentValue, WhereRuleOper.equal, cur-1)
                    .execute();
            if(updateDbRvo.getEffectRowCount()==1)
                return sysSerialNumber.getCurrentValue();
        }

        throw new NestedException(ServiceCode.FAIL, "序列号争夺过于激烈");


//        HashMap<String, Object> params = new HashMap<>();
//        params.put("beanName", beanName);
//        params.put("fieldName", fieldName);
//
//        Container session = Database.getSession();
//        try {
//            return session.executeQuery(RDBManager.getLocalSpaceConfig().getDialect().obtainQuerySerialNumberFunctionSQL(), params).getCell(0, 0);
//        } catch (Exception e) {
//            if (StringHelper.match(e.getCause().getMessage(), "(ORA-00904)|(ORA-06575)|(FUNCTION MINIANTS.createSerialNumber does not exist)", null) != null
//                    ||
//                    (e.getCause() instanceof MySQLSyntaxErrorException && ((MySQLSyntaxErrorException) e.getCause()).getSQLState().equals("42000"))
//                    ) {
//                Database.getSession().createCall(RDBManager.getLocalSpaceConfig().getDialect().obtainCreateSerialNumberFunctionSQL());
//                return session.executeQuery(RDBManager.getLocalSpaceConfig().getDialect().obtainQuerySerialNumberFunctionSQL(), params).getCell(0, 0);
//            } else {
//                throw e;
//            }
//        }
    }

    private static final long serialVersionUID = 3209654992912075385L;
    private String beanName;
    private String fieldName;
    private String currentValue;

    @Override
    public String generateId() {
        return String.format("%1$s%2$tY%2$tm%2$td%2$TH%2$TM%2$TS%2$TS%2$TL", obtainAbbreviation(), System.currentTimeMillis());
    }

    public String getBeanName() {
        return beanName;
    }

    public void setBeanName(String beanName) {
        this.beanName = beanName;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getCurrentValue() {
        return currentValue;
    }

    public void setCurrentValue(String currentValue) {
        this.currentValue = currentValue;
    }

}
