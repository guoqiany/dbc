package cn.remex.db.model;

import cn.remex.db.rsql.model.ModelableImpl;

public class SysArticleClass extends ModelableImpl {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private SysArticleClass parent;
	public SysArticleClass getParent() {
		return this.parent;
	}
	public void setParent(final SysArticleClass parent) {
		this.parent = parent;
	}

}
