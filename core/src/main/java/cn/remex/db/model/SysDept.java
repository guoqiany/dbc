package cn.remex.db.model;

import cn.remex.RemexConstants;
import cn.remex.RemexConstants.Status;
import cn.remex.db.rsql.model.ModelableImpl;

import javax.persistence.*;
import java.util.List;

/**
 * @author  GQY
 * @since  2016/1/6.
 * 组织机构表
 */
@Table(uniqueConstraints={@UniqueConstraint(columnNames="sysDeptCode")})
public class SysDept extends ModelableImpl {
    private static final long serialVersionUID = -4376652426959274020L;
    @Column(length = 30)
    private String sysDeptCode; //代号
    @Column(length = 80)
    private String sysDeptName; // 名称
    @Column(length = 50)
    private String shortName; //简称
    @Column(length = 100)
    private String engName; //英文
    @Column(length = 30)
    private String companyCode; //单位代号  暂时没有使用
    @Column(length = 30)
    private RemexConstants.OrganizationType organizationType;
//    private String userStatus;  //是否使用.. 2016年3月24日20:39:12

    private Status deptStatus;

    private String deptManagerId; //部门管理者

    private String deptManagerName; //部门管理者姓名


    @Column(length = 40)
    @ManyToOne()
    private SysDept parentUnit;//上级单位(组织机构id)
    @OneToMany(mappedBy = "parentUnit")
    private List<SysDept> subUnits;
    @ManyToMany(mappedBy = "sysDepts")
    private List<SysRole> manageRoles; //管理角色 2016年1月31日19:57:23

    public List<SysDept> getSubUnits() {
        return subUnits;
    }

    public void setSubUnits(List<SysDept> subUnits) {
        this.subUnits = subUnits;
    }



    public RemexConstants.OrganizationType getOrganizationType() {
        return organizationType;
    }

    public void setOrganizationType(RemexConstants.OrganizationType organizationType) {
        this.organizationType = organizationType;
    }

    public List<SysRole> getManageRoles() {
        return manageRoles;
    }

    public void setManageRoles(List<SysRole> manageRoles) {
        this.manageRoles = manageRoles;
    }

    public SysDept getParentUnit() {
        return parentUnit;
    }

    public void setParentUnit(SysDept parentUnit) {
        this.parentUnit = parentUnit;
    }

    public String getEngName() {
        return engName;
    }

    public void setEngName(String engName) {
        this.engName = engName;
    }

    public String getSysDeptCode() {
        return sysDeptCode;
    }

    public void setSysDeptCode(String sysDeptCode) {
        this.sysDeptCode = sysDeptCode;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getSysDeptName() {
        return sysDeptName;
    }

    public void setSysDeptName(String sysDeptName) {
        this.sysDeptName = sysDeptName;
    }

    public Status getDeptStatus() {
        return deptStatus;
    }

    public void setDeptStatus(Status deptStatus) {
        this.deptStatus = deptStatus;
    }

    public String getDeptManagerId() {
        return deptManagerId;
    }

    public void setDeptManagerId(String deptManagerId) {
        this.deptManagerId = deptManagerId;
    }

    public String getDeptManagerName() {
        return deptManagerName;
    }

    public void setDeptManagerName(String deptManagerName) {
        this.deptManagerName = deptManagerName;
    }
}
