package cn.remex.db.model;

import cn.remex.RemexConstants;
import cn.remex.RemexConstants.BlockType;
import cn.remex.RemexConstants.Status;
import cn.remex.db.rsql.model.ModelableImpl;

import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.util.List;

/**
 * Created by xuyanhua on 2017/6/15.
 */
@Table(uniqueConstraints={
        @UniqueConstraint(columnNames = {"blockType"}
        )
})
public class SysBlock extends ModelableImpl {

    private BlockType blockType;

    private String blockName;

    private String blockDescription;

    @ManyToMany(mappedBy = "blocks",targetEntity = SysRole.class)
    private List<SysRole> sysRoles;

    private Status blockStatus;

    public BlockType getBlockType() {
        return blockType;
    }

    public void setBlockType(BlockType blockType) {
        this.blockType = blockType;
    }

    public String getBlockDescription() {
        return blockDescription;
    }

    public void setBlockDescription(String blockDescription) {
        this.blockDescription = blockDescription;
    }

    public List<SysRole> getSysRoles() {
        return sysRoles;
    }

    public void setSysRoles(List<SysRole> sysRoles) {
        this.sysRoles = sysRoles;
    }

    public Status getBlockStatus() {
        return blockStatus;
    }

    public void setBlockStatus(Status blockStatus) {
        this.blockStatus = blockStatus;
    }

    public String getBlockName() {
        return blockName;
    }

    public void setBlockName(String blockName) {
        this.blockName = blockName;
    }
}
