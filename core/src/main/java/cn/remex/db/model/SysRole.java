package cn.remex.db.model;

import cn.remex.db.rsql.model.ModelableImpl;

import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.util.List;
@Table(uniqueConstraints={
		@UniqueConstraint(columnNames = {"name"}
				)
})
public class SysRole extends ModelableImpl{

	private static final long serialVersionUID = -4219376074850276724L;
	@ManyToMany(mappedBy="roles",targetEntity=SysUri.class)
	private List<SysUri> sysUris;
	@ManyToMany(mappedBy="roles",targetEntity=SysMenu.class)
	private List<SysMenu> menus;
	@ManyToMany(mappedBy="roles")
	private List<SysUser> users;
	@ManyToMany(mappedBy = "manageRoles",targetEntity = SysDept.class)
	private List<SysDept> sysDepts;
	private List<SysModule> modules;//可使用的模块列表

	private List<SysBlock> blocks;//可使用的块


	/*
	 * 允许访问的数据
	 * 根据组织结构中的部门来决定。
	 */
	public SysRole(final String name) {
		super(name);
	}
	public SysRole() {}
	public List<SysUri> getSysUris() {
		return sysUris;
	}
	public void setSysUris(List<SysUri> sysUris) {
		this.sysUris = sysUris;
	}
	public List<SysMenu> getMenus() {
		return menus;
	}
	public void setMenus(List<SysMenu> menus) {
		this.menus = menus;
	}
	public List<SysUser> getUsers() {
		return users;
	}
	public void setUsers(List<SysUser> users) {
		this.users = users;
	}

	public List<SysDept> getSysDepts() {
		return sysDepts;
	}

	public void setSysDepts(List<SysDept> sysDepts) {
		this.sysDepts = sysDepts;
	}
	public List<SysModule> getModules() {
		return modules;
	}

	public void setModules(List<SysModule> modules) {
		this.modules = modules;
	}

	public List<SysBlock> getBlocks() {
		return blocks;
	}

	public void setBlocks(List<SysBlock> blocks) {
		this.blocks = blocks;
	}
}
