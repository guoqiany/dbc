package cn.remex.db.model;

import cn.remex.db.rsql.model.ModelableImpl;

public class SysLoginLog extends ModelableImpl {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2923267653613544662L;
	private String event;
	private String logLevel;
	private String recordTime;

	public String getEvent() {
		return event;
	}
	public void setEvent(String event) {
		this.event = event;
	}
	public String getLogLevel() {
		return logLevel;
	}
	public void setLogLevel(String logLevel) {
		this.logLevel = logLevel;
	}
	public String getRecordTime() {
		return recordTime;
	}
	public void setRecordTime(String recordTime) {
		this.recordTime = recordTime;
	}

	private String ip;
	private String uri;
	private String username;
	public String getIp() {
		return this.ip;
	}
	public String getUri() {
		return this.uri;
	}
	public String getUsername() {
		return this.username;
	}
	public void setIp(final String ip) {
		this.ip = ip;
	}
	public void setUri(final String uri) {
		this.uri = uri;
	}
	public void setUsername(final String username) {
		this.username = username;
	}
}
