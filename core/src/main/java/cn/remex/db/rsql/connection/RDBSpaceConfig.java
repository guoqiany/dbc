/*
 * 文 件 名 : RDBSpaceConfig.java
 * CopyRright (c) since 2013:
 * 文件编号：
 * 创 建 人：Liu Hengyang Email:yangyang8599@163.com QQ:119316891
 * 日    期： 2013-2-24
 * 修 改 人：
 * 日   期：
 * 描   述：
 * 版 本 号： 1.0
 */

package cn.remex.db.rsql.connection;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.remex.db.Container;
import cn.remex.db.rsql.RsqlContainer;
import cn.remex.db.rsql.connection.dialect.Dialect;
import cn.remex.db.rsql.model.Modelable;
import org.springframework.beans.factory.InitializingBean;

import javax.sql.DataSource;

/**
 * @author Hengyang Liu  yangyang8599@163.com
 * @since 2013-2-24
 *
 */
public class RDBSpaceConfig implements InitializingBean {
	private Dialect dialect;
	private Class<? extends Container> containerClass = RsqlContainer.class;
	private String spaceName;
	private String type; //mssql,mysql,oracle
	private Map<String, Class<? extends Modelable>> ormBeans = new HashMap<>();
	private List<String> ormBeanPackages;
	private DataSource dataSource;
	private boolean cannotRebuild;
	private int defaultScale = 2; //默认double float的精度 TODO

	@Override
	public void afterPropertiesSet() throws Exception {
		RDBManager.createSpace(this);
	}
	public void setDialectClass(final String dialect) throws Exception {
		this.dialect = (Dialect) Class.forName(dialect).newInstance();
	}


	//getter and setter
	public void setContainerClass(Class<? extends Container> containerClass) {
		this.containerClass = containerClass;
	}
	public Class<? extends Container> getContainerClass() {
		return containerClass;
	}
	public Dialect getDialect() {
		return this.dialect;
	}
	public DataSource getDataSource() {
		return dataSource;
	}
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
	public Class<? extends Modelable> getOrmBeanClass(final String beanName) {
		return null != beanName ? this.ormBeans.get(beanName
				.split("\\$\\$EnhancerByCGLIB\\$\\$")[0]) : null;
	}
	public List<String> getOrmBeanPackages() {
		return this.ormBeanPackages;
	}
	public Map<String, Class<?>> getOrmBeans() {
		Map<String,Class<?>> ret = new HashMap<>();
		ret.putAll(this.ormBeans);
		return ret;
	}
	public String getSpaceName() {
		return this.spaceName;
	}
	public String getType() {
		return this.type;
	}
	public boolean hasOrmBeanClass(final Type type){
		if(type instanceof Class<?>) {
			return null!=getOrmBeanClass(((Class<?>) type).getSimpleName());
		}
		return false;
	}
	public void setOrmBeanPackages(final List<String> ormBeanPackages) {
		this.ormBeanPackages = ormBeanPackages;
	}
	public void setOrmBeans(final Map<String, Class<? extends Modelable>> ormBeans) {
		this.ormBeans = ormBeans;
	}
	public void setSpaceName(final String spaceName) {
		this.spaceName = spaceName;
	}
	public void setType(final String type) {
		this.type = type;
	}
	public boolean isCannotRebuild() {
		return cannotRebuild;
	}
	public void setCannotRebuild(boolean cannotRebuild) {
		this.cannotRebuild = cannotRebuild;
	}
	public int getDefaultScale() {
		return defaultScale;
	}
	public void setDefaultScale(int defaultScale) {
		this.defaultScale = defaultScale;
	}
}
