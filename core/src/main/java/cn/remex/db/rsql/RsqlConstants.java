package cn.remex.db.rsql;

import cn.remex.db.exception.RsqlExecuteException;
import cn.remex.db.utils.exception.ServiceCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public interface RsqlConstants {

	/**
	 * @author Hengyang Liu
	 * @since 2012-4-30
	 */
	enum DataStatus{
		needSave,saving,beanNew,removed,managed,part;
		public boolean equalsString(String status){
			boolean b;
			try{
				DataStatus enumStatus = DataStatus.valueOf(status);
				b  = this==enumStatus;
			}catch (Exception e) {
				throw new RsqlExecuteException(ServiceCode.RSQL_BEANSTATUS_ERROR, "数据库持久化状态异常！输入状态为:"+status,e);
			}
			return b;
		}
	}

	enum SqlOper{
		/**增加*/
		add,
		/**删除*/
		del,
		/**编辑*/
		edit,
		/**执行*/
		execute,
		/** 查看数据 **/
		list,
		/**数据存储*/
		store,
		/**数据查看*/
		view,
		/**复制*/
		copy,
		/***/
		sql
	}

	enum SysStatusEnum{
		//异步线程
		Init,Doing,Finish,Fail
		//lock 状态
		,LOCKED,UNLOCKED
	}
	/**
	 * bean的数据状态
	 **/
	/**
	 * 数据状态dataStatus
	 */
	//public static final String DS_beanOnlyId="beanOnlyId"; 已经没有了。
	public static final String DS_beanNew="beanNew";
	public static final String DS_managed="managed";
	public static final String DS_part="part";
	public static final String DS_needSave="needSave";
	
//	public static final String DS_normal="normal";
	public static final String DS_removed="removed";

//	public static final String DS_saved="saved"; //用managed替代
	public static final String DS_saving = "saving";
	/**
	 * 储存数据的基本类型
	 */
	public static final String DT_base = "bd";
	/**

	/**
	 * 完全数据模型
	 */
//	public static final String DT_whole = DT_base+DT_object+DT_objectExt+DT_collection+DT_map;
	static Logger logger= LoggerFactory.getLogger(RsqlConstants.class);
	static boolean isDebug = logger.isDebugEnabled();
	public static final String PN_bn = "bn";//beanName的缩写
//	public static final String PN_model = "model";//beanName的缩写


	public static final String SYS_createOperator = "createOperator";
//	public static final String SYS_createOperator_name = "createOperator_name";
	public static final String SYS_createTime = "createTime";
	public static final String SYS_dataStatus = "_dataStatus";
//	public static final String SYS_version = "version";
	public static final String SYS_id = "id";
	public static final String SYS_Method_setDataStatus="_setDataStatus";
	public static final String SYS_Method_setId="setId";
	public static final String SYS_modifyOperator = "modifyOperator";
//	public static final String SYS_modifyOperator_name = "modifyOperator_name";
	public static final String SYS_modifyTime = "modifyTime";
//	public static final String SYS_name = "name";//此列一般在对象中都有，用于默认检索一对一外键是显示用的
	public static final String SYS_ownership = "ownership";
//	public static final String SYS_ownership_name = "ownership_name";
}
