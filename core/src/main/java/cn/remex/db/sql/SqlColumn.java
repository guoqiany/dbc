package cn.remex.db.sql;

import cn.remex.db.DbCvo;
import cn.remex.db.lambdaapi.ColumnPredicate;
import cn.remex.db.lambdaapi.ListColumnPredicate;
import cn.remex.db.lambdaapi.ModelColumnPredicate;
import cn.remex.db.rsql.RsqlConstants;
import cn.remex.db.rsql.RsqlUtils;
import cn.remex.db.rsql.model.Modelable;
import cn.remex.db.rsql.model.ModelableImpl;
import cn.remex.db.utils.Assert;
import cn.remex.db.utils.Judgment;
import cn.remex.db.utils.Param;
import cn.remex.db.utils.exception.ServiceCode;
import cn.remex.db.utils.reflect.ReflectUtil;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

import static cn.remex.db.sql.FieldType.*;
import static cn.remex.db.sql.SqlType.getFields;

/**
 * Created by yangy on 2016/1/11 0011.
 */

/**
 * ParentType 如果是子查询,则是上一层表的类型,如果是主表,则为?
 * T 当前语句主表的类型。
 * CT 当前Column关联的Model的类型
 * ST 当前节点的属性节点的类型
 * S2T 二层子节点的类型
 * */
public class SqlColumn<ParentType extends Modelable, T extends Modelable, CT extends Modelable, ST extends Modelable> implements Serializable {

    public static <ParentType extends Modelable, T extends Modelable, CT extends Modelable, ST extends Modelable>
        void obtainFieldColumn(SqlColumn<ParentType, T, CT, ST> supNode, Object input, Consumer<SqlColumn<ParentType, T, ST, ?>> sqlColumnConsumer){
        Consumer p = propName -> {
            SqlColumn sc = new SqlColumn(supNode).init(propName.toString(), null, null, null, TBase);
            //if(null!=sc.getFieldAliasName() && sc.getFieldAliasName().indexOf(".")<0)supNode.dbCvo.addDataColumns(sc.getFieldAliasName());//TODO 兼容dataColumns指令用于保存指定的列
            if(null!=supNode)supNode.subColumns.add(sc);
            //内部链式访问
            if(null!=sqlColumnConsumer)sqlColumnConsumer.accept(sc);
        };

        Assert.notNull(input, ServiceCode.ERROR,  "输入仅接受字符串和ModelColumnPredicate lambda表达式。");
        if (input instanceof ColumnPredicate) {
            ColumnPredicate mcp = (ColumnPredicate) input;
            ReflectUtil.eachFieldWhenGet(supNode.nodeBean, c -> mcp.init((ST) c), p);
        } else {
            p.accept(input);
        }
    }
    public static <ParentType extends Modelable, T extends Modelable, CT extends Modelable, ST extends Modelable, S2T extends Modelable>
        void obtainModelColumn(SqlColumn<ParentType, T, CT, ST> supNode, Object input, Consumer<SqlColumn<ParentType, T, ST, S2T>> sqlColumnConsumer){
        Consumer p = propName -> {
            SqlColumn<ParentType, T, ST, S2T> sc = (SqlColumn<ParentType, T, ST, S2T>) supNode.getSubColumns().stream().filter(subColumn -> subColumn.getFieldName().equals(propName)).findFirst().orElse(null);
            if(null == sc){
                sc = new SqlColumn<>(supNode);
                if(null!=supNode)supNode.subColumns.add(sc);
            }
            //if(null!=sc.getFieldAliasName() && sc.getFieldAliasName().indexOf(".")<0)supNode.dbCvo.addDataColumns(sc.getFieldAliasName());//TODO 兼容dataColumns指令用于保存指定的列
            Class<S2T> nodeClass = (Class<S2T>) getFields(supNode.nodeClass, TObject).get(propName.toString());
            S2T nodeBean = ReflectUtil.createAopBean(nodeClass);
            Assert.notNull(nodeClass, ServiceCode.ERROR,  "Model属性不在模型中！");
            sc.init(propName.toString(), nodeBean, nodeClass,nodeClass, TObject);

            //内部链式访问
            if(null!=sqlColumnConsumer)sqlColumnConsumer.accept(sc);
        };

        Assert.notNull(input, ServiceCode.ERROR,  "输入仅接受字符串和ModelColumnPredicate lambda表达式。");
        if (input instanceof ModelColumnPredicate) {
            ModelColumnPredicate mcp = (ModelColumnPredicate) input;
            ReflectUtil.eachFieldWhenGet(supNode.nodeBean, c -> mcp.init((ST) c), p);
        } else {
            p.accept(input);
        }

    }
    public static <ParentType extends Modelable, T extends Modelable, CT extends Modelable, ST extends Modelable, S2T extends Modelable>
        void obtainListColumn(SqlColumn<ParentType, T, CT, ST> supNode, Object input, Consumer<SqlColumn<ParentType, T, ST, S2T>> sqlColumnConsumer){
        Consumer p = propName -> {
            SqlColumn<ParentType, T, ST, S2T> sc = (SqlColumn<ParentType, T, ST, S2T>) supNode.getSubColumns().stream().filter(subColumn -> subColumn.getFieldName().equals(propName)).findFirst().orElse(null);
            if(null == sc){
                sc = new SqlColumn<>(supNode);
                if(null!=supNode)supNode.subColumns.add(sc);
            }
            //此处的C是List类型属性的属性名
            Type listClass = SqlType.getFields(supNode.nodeClass, FieldType.TCollection).get(propName.toString());
            Class<S2T> nodeClass = (Class<S2T>) ReflectUtil.getListActualType(listClass);
            Assert.notNull(nodeClass, ServiceCode.ERROR,  "List属性不在模型中！");
            S2T nodeBean = ReflectUtil.createAopBean(nodeClass);
            sc.init(propName.toString(), nodeBean, nodeClass, listClass, FieldType.TCollection);

            //内部链式访问
            if(null!=sqlColumnConsumer)sqlColumnConsumer.accept(sc);
        };

        Assert.notNull(input, ServiceCode.ERROR,  "输入仅接受字符串和ModelColumnPredicate lambda表达式。");
        if (input instanceof ListColumnPredicate) {
            ListColumnPredicate mcp = (ListColumnPredicate) input;
            ReflectUtil.eachFieldWhenGet(supNode.nodeBean, c -> mcp.init((ST) c), p);
        } else {
            p.accept(input);
        }
    }


    //===================================//
    //默认情况下SqlColumn的FilterBy操作是在On子句下面的，如果是从DbCvo出发的则需要将开关设置为true，代表接下来的filter*操作将作用域顶层的Where子句
    //LHY 2016-12-15 修改默认情况放到Where
    private boolean _switchFilterToDbCvo = true;
    //LHY 2017-2-1 不用查询或更新
    private boolean _notWith = false;

    private Where<ParentType, T,CT> obtainCurFilter() {
        return _isSwitchFilterToDbCvo() ? (filter == null ? filter = new Where<>() : filter) : (onFilter == null ? onFilter = new Where<>() : onFilter);
    }

    //===================================//
    private FieldType type;
    /**
     * 当前接的父属性节点。如果是当前表的属性列，则父节点为空。
     */
    private SqlColumn supColumn;
    /**
     * 当前节点属性的类型。如果是基本列，则nodeClass为null；如果是Model列或Collection列则是ModelClass
     */
    private Class<ST> nodeClass;
    /**
     * 当前节点属性的类型，如果是基本列，则nodeClass为null；如果是Model列则是ModelClass；如果是Collection列则是List<ModelClass>
     */
    private Type nodeType;
    private ST nodeBean;
    private DbCvo<T, ParentType> dbCvo;
    private String fieldName;
    private AggregateFunction aggregateFunction;
    private String aggregateAliasName;
    private GroupFunction groupFunction;
    private String groupAliasName;
    private Object[] aggregateFunArgs;
    private Object[] groupFunctionArgs;
    private boolean isGroupBy = false;
    private boolean distinct;

    private List<SqlColumn<ParentType,T,ST,?>> subColumns = new ArrayList<>();

    //当是面向对象的属性时，则有表名，虚拟表名
    private String tableName;
    private String aliasName;//虚拟表名只能再select实例化并调用去生成sql时才能产生
    private String fieldAliasName;//init时产生，init函数必须调用，否则功能会出错。

    //子查询 仅支持一个，目前用于//用于对列赋值的dbCvo，一般为子查询
    private DbCvo subDbCvo;

    private Where<ParentType,T,CT> filter; // dbCvo顶层Where子句的filter
    private Where<ParentType,T,CT> onFilter;//on子句的filter
    /*构造及初始化函数*/
    public SqlColumn(DbCvo<T, ParentType> dbCvo, Class<ST> beanClass, ST t) {
        this.dbCvo = dbCvo;
        this.supColumn = null;
        this.type = TROOT;
        this.nodeType = beanClass;
        this.nodeClass = beanClass;
        this.nodeBean = t;

        //处理好列的alaisName，用途：1.反序列化为对象时，需要根据此表达式填充到JavaBean中；2.where toSql根据此名来判断需要搜索的列是否在SQL语句中
		//凡是model or list属性是不用加fieldName的。
        this.fieldAliasName = null==supColumn?this.fieldName:obtainChain()+(TBase==type?this.fieldName:"$");
        if(this.fieldAliasName!=null && this.fieldAliasName.endsWith(".$")) this.fieldAliasName = this.fieldAliasName.substring(0, this.fieldAliasName.length() - 2);
    }

    public SqlColumn(SqlColumn supColumn) {
        this.dbCvo = supColumn.dbCvo;
        this.supColumn = supColumn;
    }
    public SqlColumn<ParentType, T, CT, ST> init(String fieldName, ST nodeBean, Class<ST> nodeClass, Type nodeType, FieldType type) {
        this.type = type;
        this.fieldName = fieldName;
        this.nodeType = nodeType;
        this.nodeClass = nodeClass;
        this.nodeBean = nodeBean;

        //处理好列的alaisName，用途：1.反序列化为对象时，需要根据此表达式填充到JavaBean中；2.where toSql根据此名来判断需要搜索的列是否在SQL语句中
		//凡是model or list属性是不用加fieldName的。
        this.fieldAliasName = null==supColumn?this.fieldName:obtainChain()+(TBase==type?this.fieldName:"$");
        if(this.fieldAliasName!=null && this.fieldAliasName.endsWith(".$")) this.fieldAliasName = this.fieldAliasName.substring(0, this.fieldAliasName.length() - 2);
        return this;
    }


    /*lambda链式访问函数*/
    public StringBuilder obtainChain() {
        if(TROOT.equals(type)) {
            return new StringBuilder();
        }else{
            // [].用于描述list属性下的引用   .用于描述Model属性下的引用
            StringBuilder sb = supColumn.obtainChain();
            if(TBase.equals(type)){
                //;
            }else {
                sb.append(fieldName).append(TCollection.equals(type) ? "[]." : ".");
            }
            return sb;
        }
    }


	//可以通过field.field.baseField 的方式来调用相关列
    public SqlColumn<ParentType, T, CT, ST> withColumn(String fieldAliasName) {
        Assert.notNullAndEmpty(fieldAliasName,ServiceCode.RSQL_SQL_ERROR,"field表达式不能为空");
        Param<Boolean> hasFound = new Param(false);
        int flag;
        String fieldName = (flag= fieldAliasName.indexOf('.'))>=0? fieldAliasName.substring(fieldAliasName.indexOf('.')+1): fieldAliasName;

        //插在该列是否被dbCvo使用并存在
        anySubColumnMatch(
                p -> !Judgment.nullOrBlank(p.getFieldAliasName()) && fieldAliasName.equals(p.getFieldAliasName()),
                c -> {
                    if (flag > 0) {
                        c.withColumn(fieldAliasName.substring(flag));
                    }
                    hasFound.param = true;
                });
        if(!hasFound.param){
            //如果不存在则通过with添加上,先判断是model还是list
            Type parentFieldType = SqlType.getFields(getNodeClass(), FieldType.TAll).get(fieldName);
            Assert.notNull(parentFieldType, ServiceCode.RSQL_SQL_ERROR, "where 查询的列不属于对象的属性:"+fieldName);
            if(SqlType.isTObject(parentFieldType)){
                withModel(fieldName, c -> {
                    if (flag > 0) {
                        c.withColumn(fieldAliasName.substring(flag));
                    }
                    hasFound.param = true;
                });
            }else if(SqlType.isTCollection(parentFieldType)){
                withList(fieldName, c -> {
                    if (flag > 0) {
                        c.withColumn(fieldAliasName.substring(flag));
                    }
                    hasFound.param = true;
                });
            }else {
                withBase(fieldName);
            }
        }
        return this;
    }

    public SqlColumn<ParentType,T, CT, ST> withBase(ColumnPredicate<ST> cp, Consumer<SqlColumn<ParentType, T, ST, ?>> sqlColumnConsumer) {
        obtainFieldColumn(this, cp, sqlColumnConsumer);
        return this;
    }
    public SqlColumn<ParentType, T, CT, ST> withBase() {
        //添加本节点所有基本TBase列到Sql语句中
        SqlType.getGetters(this.nodeClass,TBase).forEach((k, v) -> {
            if(!RsqlUtils.SysColumns.containsKey(k) || RsqlConstants.SYS_id.equals(k))
                obtainFieldColumn(this, k, null);
        });
        return this;
    }
    public SqlColumn<ParentType, T, CT, ST> withBase(ColumnPredicate<ST> cp) {
        obtainFieldColumn(this, cp, null);
        return this;
    }
    public SqlColumn<ParentType, T, CT, ST> withBase(String fieldName, Consumer<SqlColumn<ParentType, T, ST, ?>> sqlColumnConsumer) {
        obtainFieldColumn(this, fieldName, sqlColumnConsumer);
        return this;
    }
    public SqlColumn<ParentType, T, CT, ST> withBase(String fieldName) {
        obtainFieldColumn(this, fieldName, null);
        return this;
    }
    public <S2T extends Modelable> SqlColumn<ParentType, T, CT, ST> withModel(ModelColumnPredicate<T, ST, S2T> mcp, Consumer<SqlColumn<ParentType, T, ST, S2T>> sqlColumnConsumer) {
        obtainModelColumn(this, mcp, sqlColumnConsumer);
        return this;
    }
    public <S2T extends Modelable> SqlColumn<ParentType, T, CT, ST> withModel(ModelColumnPredicate<T, ST, S2T> mcp) {
        obtainModelColumn(this, mcp, null);
        return this;
    }
    public <S2T extends Modelable> SqlColumn<ParentType, T, CT, ST> withModel(String fieldName, Consumer<SqlColumn<ParentType, T, ST, S2T>> sqlColumnConsumer) {
        obtainModelColumn(this, fieldName, sqlColumnConsumer);
        return this;
    }
    public <S2T extends Modelable> SqlColumn<ParentType, T, CT, ST> withModel(String fieldName) {
        obtainModelColumn(this, fieldName, null);
        return this;
    }
    public <S2T extends Modelable> SqlColumn<ParentType, T, CT, ST> withList(ListColumnPredicate<T, ST, S2T> lcp, Consumer<SqlColumn<ParentType, T, ST, S2T>> sqlColumnConsumer) {
        obtainListColumn(this, lcp, sqlColumnConsumer);
        return this;
    }
    public <S2T extends Modelable> SqlColumn<ParentType, T, CT, ST> withList(ListColumnPredicate<CT, ST, S2T> lcp) {
        obtainListColumn(this, lcp, null);
        return this;
    }
    public <S2T extends Modelable> SqlColumn<ParentType, T, CT, ST> withList(String fieldName, Consumer<SqlColumn<ParentType, T, ST, S2T>> sqlColumnConsumer) {
        obtainListColumn(this, fieldName, sqlColumnConsumer);
        return this;
    }
    public <S2T extends Modelable> SqlColumn<ParentType, T, CT, ST> withList(String fieldName) {
        obtainListColumn(this, fieldName, null);
        return this;
    }
    public SqlColumn<ParentType, T, CT, ST> on(Consumer<SqlColumn<ParentType, T, CT, ST>> sqlColumnConsumer) {
        _setSwitchFilterToDbCvo(false);//将sqlColumn操作的filter调整为dbCvo的Filter，也就是说在此lambada语句里面的filter将会列入Where子句
        sqlColumnConsumer.accept(this);
        _setSwitchFilterToDbCvo(true);//将sqlColumn操作的filter调整为SqlColumn的Filter，设置回去Filter改回ON子句中
        return this;
    }
//    public SqlColumn<ParentType, T, CT, ST> filterBy(ColumnPredicate<ST> wp, WhereRuleOper oper, Object value) {
//        ReflectUtil.eachFieldWhenGet(nodeBean, b -> wp.init((ST) b), s -> obtainCurFilter().addRule(s, oper, value));
//        return this;
//    }
    public SqlColumn<ParentType, T, CT, ST> filterBy(Consumer<Where<ParentType, T, CT>> filterConsumer) {
        filterConsumer.accept(obtainCurFilter());
        return this;
    }
    public <S2T extends Modelable> SqlColumn<ParentType, T, CT, ST> filterByModel(ModelColumnPredicate<T, ST, S2T> mcp, Consumer<SqlColumn<ParentType, T, ST, S2T>> sqlColumnConsumer) {
        obtainModelColumn(this, mcp, sqlColumn->{
            sqlColumnConsumer.accept((SqlColumn<ParentType, T, ST, S2T>)sqlColumn);//内部处理
        });
        return this;
    }
    public <S2T extends Modelable> SqlColumn<ParentType, T, CT, ST> filterByList(ListColumnPredicate<T, ST, S2T> lcp, Consumer<SqlColumn<ParentType, T, ST, S2T>> sqlColumnConsumer) {
        obtainListColumn(this, lcp, sqlColumn->{
            sqlColumnConsumer.accept((SqlColumn<ParentType, T, ST, S2T>)sqlColumn);//内部处理
        });
        return this;
    }
    public <SSB extends ModelableImpl> SqlColumn<ParentType, T, CT, ST> filterBy(ColumnPredicate<ST> wp, WhereRuleOper oper, Class<SSB> subSelectBeanClass, Consumer<DbCvo<SSB, T>> subSelectSqlColumnConsumer){
        DbCvo<SSB, T> subSelectDbCvo = new DbCvo<>(dbCvo._getSpaceName(),subSelectBeanClass,true);
        subSelectSqlColumnConsumer.accept(subSelectDbCvo);
        ReflectUtil.eachFieldWhenGet(nodeBean, b -> wp.init((ST) b), s -> obtainCurFilter().addSubSelectRule(s, oper, subSelectDbCvo));
        return this;
    }
    public <SSB extends ModelableImpl> SqlColumn<ParentType, T, CT, ST> filterBy(ColumnPredicate<ST> wp, WhereRuleOper oper, Object... args){
        ReflectUtil.eachFieldWhenGet(nodeBean, b -> wp.init((ST) b), s -> obtainCurFilter().addRule(s, oper, args));
        return this;
    }
    public <SSB extends ModelableImpl> SqlColumn<ParentType, T, CT, ST> filterByGroup(Consumer<Where<ParentType, T, ST>> groupConsumer) {
        //处理filterByList、filterByModel中没有子sub的问题
        if(subDbCvo==null)
            this.subDbCvo = new DbCvo<ST, T>(this.dbCvo._getSpaceName(), this.nodeClass, true);


        Where<ParentType, T, ST> group = new Where<>();
        group.setSuperDbCvo(this.subDbCvo);
        group.setSuperWhere((Where<ParentType, T, ST>) obtainCurFilter());
        obtainCurFilter().addGroup(group);

        groupConsumer.accept(group);

        return this;
//        this.filter.setSuperDbCvo(this);
//        Where<T, T> group = new Where<>();
//        group.setSuperDbCvo(this);
//        group.setSuperWhere(this.filter);
//        this.filter.addGroup(group);
//        return group;
    }

    public SqlColumn<ParentType, T, CT, ST> filterOper(WhereGroupOp groupOp) {
        obtainCurFilter().setGroupOp(groupOp);
        return this;
    }
    public SqlColumn<ParentType, T, CT, ST> groupBy(){
        dbCvo._setHasGroupBy(true);
        setIsGroup(true);
        return this;
    }
    public SqlColumn<ParentType, T, CT, ST> groupBy(GroupFunction groupFunction,Object... groupFunArgs){
        dbCvo._setHasGroupBy(true);
        setIsGroup(true);
        switch (groupFunction){
            case DateStr2Day:
                setGroupFunction(GroupFunction.SUBSTR);
                setGroupFunctionArgs(new Object[]{1,10});
                break;
            case DateToDayStr:
                setGroupFunction(GroupFunction.DATE_FORMAT);
                setGroupFunctionArgs(new Object[]{"%Y-%m-%d"});
                break;
            case DateToHourStr:
                setGroupFunction(GroupFunction.DATE_FORMAT);
                setGroupFunctionArgs(new Object[]{"%Y-%m-%d %k-00-00"});
                break;
            case DateToMinuteStr:
                setGroupFunction(GroupFunction.DATE_FORMAT);
                setGroupFunctionArgs(new Object[]{"%Y-%m-%d %k-%i-00"});
                break;
            case DateFormatter:
                setGroupFunction(GroupFunction.DATE_FORMAT);
                setGroupFunctionArgs(groupFunArgs);
                break;
            default:
                setGroupFunction(groupFunction);
                setGroupFunctionArgs(groupFunArgs);
        }



        if(this.isGroupBy() && null!=this.groupFunction && !Judgment.nullOrBlank(this.groupAliasName)){
            //处理别名
            this.fieldAliasName = this.fieldAliasName.trim()+ ("#"+this.groupFunction);
            if(null != groupFunctionArgs) {
                for (Object arg : groupFunctionArgs) {
                    this.fieldAliasName += ("_" + arg);
                }
            }
        }

        return this;
    }
    public SqlColumn<ParentType, T, CT, ST> groupAlias(String groupAliasName) {
        this.groupAliasName = groupAliasName;
        return this;
    }
    public SqlColumn<ParentType, T, CT, ST> aggregateBy(AggregateFunction fun){
        dbCvo._setHasAggregateBy(true);
        aggregateFunction = fun;
        return this;
    }

    public SqlColumn<ParentType, T, CT, ST> aggregateBy(AggregateFunction fun,Object... aggregateFunArgs){
        dbCvo._setHasAggregateBy(true);
        aggregateFunction = fun;
        switch (fun){
            case CASE_SUM:
                setAggregateFunArgs(aggregateFunArgs);
                break;
        }
//        if(this.isGroupBy() && null!=this.groupFunction && !Judgment.nullOrBlank(this.groupAliasName)){
//            //处理别名
//            this.fieldAliasName = this.fieldAliasName.trim()+ ("#"+this.groupFunction);
//            if(null != groupFunctionArgs) {
//                for (Object arg : groupFunctionArgs) {
//                    this.fieldAliasName += ("_" + arg);
//                }
//            }
//        }
        return this;
    }
    public SqlColumn<ParentType, T, CT, ST> aggregateAlias(String aliasAggrName){
        aggregateAliasName = aliasAggrName;
        return this;
    }

    public SqlColumn<ParentType, T, CT, ST> distinct() {
        dbCvo._setHasDistinct(true);
        this.distinct = true;
        return this;
    }
    public SqlColumn<ParentType, T, CT, ST> alias(String fieldAliasName) {
        this.setFieldAliasName(fieldAliasName);
        return this;
    }

    public DbCvo<T, ParentType> dbCvo() {
        return this.dbCvo;
    }

    /*
     * 遍历每个节点，含子节点
     */
    public SqlColumn<ParentType, T, CT, ST> forEvery(Consumer<SqlColumn> c) {
        c.accept(this);
        if(null!=subColumns)subColumns.forEach(cc->cc.forEvery(c));
        return this;
    }
    public SqlColumn<ParentType, T, CT, ST> subColumn(String fieldName) {
        return (anyMatchResult(sqlColumn -> null!=sqlColumn.getSupColumn() && TROOT.equals(sqlColumn.getSupColumn().getType()) && null!=sqlColumn.getFieldName() && sqlColumn.getFieldName().equals(fieldName)));
    }
    public boolean anyMatchInRoot(String fieldName) {
        return null != subColumn(fieldName);
    }
    private SqlColumn<ParentType, T, CT, ST> anyMatchResult(Predicate<SqlColumn> p) {
        if(p.test(this)){
            return this;
        }
        if(null!=subColumns)
            for(SqlColumn sqlColumn:subColumns){
                if(sqlColumn.anyMatchResult(p)!=null) return sqlColumn;
            }
        return null;
    }
    public SqlColumn<ParentType, T, CT, ST> anyMatchInRoot(Predicate<SqlColumn> p, Consumer<SqlColumn> c) {
        if(p.test(this)){
            c.accept(this);
            return this;
        }
        anySubColumnMatch(p, c);
        return this;
    }
    public SqlColumn<ParentType, T, CT, ST> anySubColumnMatch(Predicate<SqlColumn> p, Consumer<SqlColumn> c) {
        SqlColumn sc;
        if(null!=subColumns)
            for(SqlColumn sqlColumn:subColumns){
                if(null!=(sc= sqlColumn.anyMatchResult(p) )){
                    c.accept(sc);
                    break;
                }
            }
        return this;
    }
    public SqlColumn supColumn() {
        return this.supColumn;
    }
    public boolean hasAnyColumn(){
        return getSubColumns() != null && getSubColumns().size() > 0;
    }

    //getter setter
    public SqlColumn getSupColumn() {
        return supColumn;
    }

    public void setSupColumn(SqlColumn supColumn) {
        this.supColumn = supColumn;
    }

    public Class<ST> getNodeClass() {
        return nodeClass;
    }

    public void setNodeClass(Class<ST> nodeClass) {
        this.nodeClass = nodeClass;
    }

    public ST getNodeBean() {
        return nodeBean;
    }

    public void setNodeBean(ST nodeBean) {
        this.nodeBean = nodeBean;
    }

    public DbCvo<T, ParentType> getDbCvo() {
        return dbCvo;
    }

    public void setDbCvo(DbCvo<T, ParentType> dbCvo) {
        this.dbCvo = dbCvo;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public Where<ParentType, T,CT> getFilter() {
        return this.filter;
    }

    public void setFilter(Where<ParentType, T,CT> filter) {
        this.filter = filter;
    }
    public FieldType getType() {
        return type;
    }

    public void setType(FieldType type) {
        this.type = type;
    }

    public String getFieldAliasName() {
        return fieldAliasName;
    }

    public void setFieldAliasName(String fieldAliasName) {
        this.fieldAliasName = fieldAliasName;
    }

    public List<SqlColumn<ParentType, T, ST, ?>> getSubColumns() {
        return subColumns;
    }

    public void setSubColumns(List<SqlColumn<ParentType, T, ST, ?>> subColumns) {
        this.subColumns = subColumns;
    }

    public Type getNodeType() {
        return nodeType;
    }

    public void setNodeType(Type nodeType) {
        this.nodeType = nodeType;
    }

    public AggregateFunction getAggregateFunction() {
        return aggregateFunction;
    }

    public void setAggregateFunction(AggregateFunction aggregateFunction) {
        this.aggregateFunction = aggregateFunction;
    }

    public String getAggregateAliasName() {
        return aggregateAliasName;
    }

    public void setAggregateAliasName(String aggregateAliasName) {
        this.aggregateAliasName = aggregateAliasName;
    }

    public boolean isGroupBy() {
        return isGroupBy;
    }

    public void setIsGroup(boolean isGroupBy) {
        this.isGroupBy = isGroupBy;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public GroupFunction getGroupFunction() {
        return groupFunction;
    }

    public void setGroupFunction(GroupFunction groupFunction) {
        this.groupFunction = groupFunction;
    }

    public Object[] getGroupFunctionArgs() {
        return groupFunctionArgs;
    }

    public void setGroupFunctionArgs(Object[] groupFunctionArgs) {
        this.groupFunctionArgs = groupFunctionArgs;
    }

    public Object[] getAggregateFunArgs() {
        return aggregateFunArgs;
    }

    public void setAggregateFunArgs(Object[] aggregateFunArgs) {
        this.aggregateFunArgs = aggregateFunArgs;
    }

    public Where<ParentType, T, CT> getOnFilter() {
        return this.onFilter;
    }

    public void setOnFilter(Where<ParentType, T, CT> onFilter) {
        this.onFilter = onFilter;
    }

    public boolean _isSwitchFilterToDbCvo() {
        return _switchFilterToDbCvo;
    }

    public void _setSwitchFilterToDbCvo(boolean _switchFilterToDbCvo) {
        this._switchFilterToDbCvo = _switchFilterToDbCvo;
    }

    public String getGroupAliasName() {
        return groupAliasName;
    }

    public void setGroupAliasName(String groupAliasName) {
        this.groupAliasName = groupAliasName;
    }

    public DbCvo getSubDbCvo() {
        return subDbCvo;
    }

    public void setSubDbCvo(DbCvo subDbCvo) {
        this.subDbCvo = subDbCvo;
    }

    public boolean _isNotWith() {
        return _notWith;
    }

    public void _setNotWith(boolean _notSelect) {
        this._notWith = _notSelect;
    }
}
