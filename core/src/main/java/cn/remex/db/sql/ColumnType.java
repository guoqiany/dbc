package cn.remex.db.sql;

import javax.persistence.Column;
import java.sql.Types;

/**
 * Created by yangy on 2016/1/22 0022.
 */
public class ColumnType {
	//以下属性为Column参数属性
	public int length = 50;
	public int type = Types.CHAR;
	public int scale = 0;
	public boolean nullable = true;

//	以下属性为视图参数
//	public EditType editType;
//	public Class<?> codeRefBean;
//	public String codeRefTypeColumn;
//	public String codeRefCodeColumn;
//	public String codeRefDescColumn;
//	public String codeRefCodeType;
//	public String codeRefFilters;

	public ColumnType(int type, int length) {
		this.type = type;
		this.length = length;
	}
	public ColumnType(int type, int length, int scale) {
		this.type = type;
		this.length = length;
		this.scale = scale;
	}
	public ColumnType(int type, int length, int scale, boolean nullable) {
		this.type = type;
		this.length = length;
		this.scale = scale;
		this.nullable = nullable;
	}

	public ColumnType(ColumnType preDefineColumnType, Column sta) {
		//preDefineColumnType 来自RsqlUtils定义的，是系统级的；RDBSpaceConfig 定义的是会话级别的,会话级别的需要 TODO ；Column定义的是表里面的字段级的
		//sqlType类型定义的优先级：1.model自己定义的注解2.sqlUtil类中定义的java类型与sql类型的对应关系 3.jpa中column的默认值
		boolean notNumber = preDefineColumnType.type!=Types.NUMERIC
				&& preDefineColumnType.type!=Types.INTEGER
				&& preDefineColumnType.type!=Types.DOUBLE
				&& preDefineColumnType.type!=Types.TINYINT
				&& preDefineColumnType.type!=Types.FLOAT;
		if(notNumber){
			this.length = sta.length() != 255 ? sta.length() : preDefineColumnType.length;
		}else{//长度,如果定了数字的长度，则使用precision数字长度
			this.length = sta.precision()!=0?sta.precision():sta.length() != 255?sta.length():preDefineColumnType.length;
		}

		this.scale = sta.scale()==0?preDefineColumnType.scale:sta.scale();//精度
		this.nullable = sta.nullable();
		if("CLOB".equals(sta.columnDefinition()))
			this.type = Types.CLOB;
		else
			this.type = preDefineColumnType.type;//Types下的变量，java定义的数据字段类型
	}

}
