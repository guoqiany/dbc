package cn.remex.db.sql;

import cn.remex.db.DbCvo;
import cn.remex.db.rsql.model.Modelable;

import java.io.Serializable;

public class WhereRule implements Serializable {
	private static final long serialVersionUID = 1487114736707926155L;
	private Object data;    //选择的查询值
	private String field;    //查询字段
	private String op;        //查询操作
	private String paramName; // 命名参数名称

	//这个构造函数必须有，否则前端filter无法生成
	public WhereRule () {
	}

	public WhereRule(final String field, final WhereRuleOper ruleOper, Object data) {
		super();
		this.field = field;
		this.op = ruleOper.toString();
		//如果sql明媚参数是唯一的一个值，需要将他转化为对象而不是数据，否则在setParam时会吧对象的地址赋值给sql
		if(null != data && Object[].class.isAssignableFrom(data.getClass())){
			if(((Object[])data).length==0){
				data=null;
			}else if(((Object[])data).length==1){
				data = ((Object[]) data)[0];
			}
		}
		this.data = data==null?null:data instanceof Modelable?((Modelable) data).getId():data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	public Object getData() {
		return this.data;
	}
	public String getField() {
		return this.field;
	}
	public String getOp() {
		return this.op;
	}
	public String getParamName() {
		return this.paramName;
	}
	public void setData(final String data) {
		this.data = data;
	}
	public void setField(final String field) {
		this.field = field;
	}
	public void setOp(final String op) {
		this.op = op;
	}
	public void setParamName(final String paramName) {
		this.paramName = paramName;
	}

	public boolean isSubStatment() {
		return WhereRuleOper.in.toString().equals(getOp())
				|| WhereRuleOper.notIn.toString().equals(getOp())
				|| WhereRuleOper.exist.toString().equals(getOp())
				|| WhereRuleOper.notExist.toString().equals(getOp())
				;
	}
}

