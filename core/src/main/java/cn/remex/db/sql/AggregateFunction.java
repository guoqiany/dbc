package cn.remex.db.sql;

/**
 * Created by yangy on 2016/1/16 0016.
 */
public enum AggregateFunction {
    MAX,MIN,SUM,COUNT,AVG,

    CASE_SUM,

    Field_MAX,Field_MIN,Field_SUM,Field_COUNT,Field_AVG,


    GROUP_CONCAT_DISTINCT
}
