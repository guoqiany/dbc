package cn.remex.db.sql;

/**
 * 名称：
 * 缩写：
 * 用途：
 * Created by yangy on 2016/8/21 0021.
 */
public class Expression {
	private String expressionString;
	public Expression(String expressionString) {
		this.expressionString = expressionString;
	}

	public String formatForWhereRule(String... fields) {
		return String.format(expressionString, fields);
	}
}
