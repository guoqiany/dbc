package cn.remex.db.sql;

/**
 * Created by LIU on 15/12/1.
 */

/**
 * 条件查询的过滤规则
 */
public enum WhereRuleOper {
    /**
     * 不以...开始
     */
    notStartsWith,
    /**
     * 以...开始
     */
    startsWith,
    /**
     * 包含
     */
    contains,
    /**
     * 不以...结束
     */
    notEndsWith,
    /**
     * 等于
     */
    equal,
    /**
     * 以...结束
     */
    endsWith,
    /**
     * 大于等于
     */
    ge,
    /**
     * 大于
     */
    gt,
    /**
     * 小于等于
     */
    le,
    /**
     * 小于
     */
    lt,
    /**
     * 不包含
     */
    notContains,
    /**
     * 不等于
     */
    notEqual,
    /**
     * 在....里面
     */
    _in,
    /**
     * 不在...里面
     */
    _ni,
    /**
     * 为空
     */
    isNull,
    in, exist, notIn, notExist, /**
     * 不为空
     */
    notNull
   
}
