/**
 * 
 */
package cn.remex;


import org.apache.oro.text.regex.PatternCompiler;
import org.apache.oro.text.regex.Perl5Compiler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * @author Hengyang Liu
 * @since 2012-4-3
 *
 */
public interface RemexConstants {
	Logger logger = LoggerFactory.getLogger(RemexConstants.class);

	boolean loggerDebug = logger.isDebugEnabled();

	PatternCompiler compiler = new Perl5Compiler();

	enum UserType {
		SYSTEM, ADMIN, C_USER, B_USER
	}
	enum OrganizationType{
		SCHOOLZONE,
		COMPANY,
		PARTMENT
	}

	enum Status{
		ENABLE,//可用
		DISABLE//不可用
	}
	enum ConfigStatus{
		OPEN,//开启
		CLOSE//禁用
	}

	enum EffectFlag{//用户是否有效
		VALID,//有效
		INVALID//无效
	}

	enum BlockType{
		COMPANY_TODAY_DATA,
		CURR_LANDER_TODAY_DATA,
		COMPANY_RECENT_DATA,
		BD_DAILY_DATA,
		JOB_QUEUE_DATA

	}
}
